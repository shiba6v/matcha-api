require 'aws-sdk'
#require 'rmagick'

class PictureController < ApplicationController
	protect_from_forgery except: :create

  def index

  	pictures = []
  	Picture.all.each do |picture|
  		pictures << {id: picture.id, latitude: picture.latitude, longitude: picture.longitude, url_icon: picture.url_icon, url_image: picture.url_image}
  	end
  	render json: {images: pictures}
  end

  def create
  	puts "--------------"
  	puts "params[:latitude]: " + params[:latitude]
  	puts "params[:longitude]: " + params[:longitude]
    puts params[:byte_data]
  	puts "--------------"


  	picture = Picture.new(latitude: params[:latitude], longitude: params[:longitude], url_icon: nil,url_image: nil)
  	picture.save!

  	puts "--------------"
  	puts "@current_user.id: " + @current_user.id.to_s 
  	puts "picture.id: " + picture.id.to_s
  	puts "time: "  + Time.now.to_i.to_s
  	puts "--------------"

  	#名前の処理
  	picture_suffix = "user_" + @current_user.id.to_s + "_picture_" + picture.id.to_s + "_at_" + Time.now.to_i.to_s + ".jpg"
  	icon_suffix = PICTURE_DIR + "icon_" + picture_suffix
  	image_suffix = PICTURE_DIR + "image_" + picture_suffix
  	picture.url_icon = AWS_S3_URL + icon_suffix
  	picture.url_image = AWS_S3_URL + image_suffix

  	picture.save!

  	image_data = params[:byte_image].tempfile

  	s3 = Aws::S3::Resource.new(region:'ap-northeast-1', credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'], ENV['AWS_SECRET_KEY'] ))
    bucket = s3.bucket(BUCKET_NAME)
    obj = bucket.object(image_suffix)
    obj.upload_file(image_data)

    if params[:byte_icon]
        icon_data = params[:byte_icon].tempfile 
        obj = bucket.object(icon_suffix)
        obj.upload_file(icon_data)
    else
        picture.url_icon = picture.url_image
        picture.save!
    end

    # クライアント側で圧縮してもらうが、もしiconがなかったらimageをiconとしてつかう。

  	render json: {message: "画像の送信に成功しました。", id: picture.id, url_icon: picture.url_icon, url_image: picture.url_image}
  end
end
