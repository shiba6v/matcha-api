Rails.application.routes.draw do
  get 'picture/index'

  post 'picture', to: 'picture#create'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'sign_up', to: 'user#sign_up'
end
