class CreatePictures < ActiveRecord::Migration[5.0]
  def change
    create_table :pictures do |t|
      t.float :latitude
      t.float :longitude
      t.string :url_icon
      t.string :url_image

      t.timestamps
    end
  end
end
