class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name, default: '旅行者'
      t.string :user_id
      t.integer :point

      t.timestamps
    end
  end
end
