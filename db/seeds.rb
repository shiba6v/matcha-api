# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#八坂神社
Picture.create(latitude: 35.003656, longitude:135.778553, url_icon: "http://www.y-morimoto.com/jinja22/yasaka11.jpg", url_image: "http://www.y-morimoto.com/jinja22/yasaka11.jpg")

#よしもと祇園花月
Picture.create(latitude: 35.005009, longitude:135.778518, url_icon: "http://blog-imgs-30.fc2.com/i/n/i/inish/IMG_1524.jpg", url_image: "http://blog-imgs-30.fc2.com/i/n/i/inish/IMG_1524.jpg")

#河原町駅
Picture.create(latitude: 35.003751, longitude:135.768749, url_icon: "http://blog-imgs-93.fc2.com/e/k/i/ekimaetanbou/2016052910204354c.jpg", url_image: "http://blog-imgs-93.fc2.com/e/k/i/ekimaetanbou/2016052910204354c.jpg")

#清水寺
Picture.create(latitude: 34.994856, longitude:135.785046, url_icon: "http://i.samurai-japan.co/img/upfiles/pic/pc/kiyomizuderayuyake1.jpg", url_image: "http://i.samurai-japan.co/img/upfiles/pic/pc/kiyomizuderayuyake1.jpg")

#サポーターズ
Picture.create(latitude: 35.003837, longitude:135.760947, url_icon: "http://www.elitz.jp/img_a/webphoto/010520.jpg", url_image: "http://www.elitz.jp/img_a/webphoto/010520.jpg")

